package com.project.test;

import com.project.test.model.ResponseClass;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface NetworkCall {
    @FormUrlEncoded
    @POST("users")
    Call<ResponseClass> savePost(@Field("selectedLocation") String selectedLocation,
                                 @Field("latitute") String latitute,
                                 @Field("longitutue") String longitute);
}
