package com.project.test.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.project.test.R;
import com.project.test.service.MyService;

public class MainActivity extends AppCompatActivity {
    Button btnTap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //service started for price calculation in every 1 minute
        startService(new Intent(this, MyService.class)); //start service which is MyService.java

        btnTap = findViewById(R.id.btnTap);
        btnTap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MapsActivity.class));
            }
        });
    }
}