package com.project.test.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.test.service.MyService;
import com.project.test.NetworkCall;
import com.project.test.NotificationWorker;
import com.project.test.R;
import com.project.test.model.ResponseClass;
import com.project.test.utils.UtilClass;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.fragment.app.FragmentActivity;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    public static final String MESSAGE_STATUS = "message_status";

    private GoogleMap mMap;
    private static final String TAG = "MapsActivity";
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    private NetworkCall mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mAPIService = UtilClass.getAPIService();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//workerClass for background work
//        final WorkManager mWorkManager = WorkManager.getInstance();
//        PeriodicWorkRequest refreshWork =
//                new PeriodicWorkRequest.Builder(NotificationWorker.class, 15, TimeUnit.MINUTES).build();
//        mWorkManager.enqueue(refreshWork);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(latLng.latitude + ":" + latLng.longitude);
                Log.wtf("latitude", String.valueOf(latLng.latitude));
                Log.wtf("longitude", String.valueOf(latLng.longitude));
                String locationName = getAddress(MapsActivity.this, latLng.latitude, latLng.longitude);
                Log.wtf("locationName ", locationName);

                //Api calling for sending location to server
                sendPost(locationName, String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));

                mMap.clear();
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                mMap.addMarker(markerOptions);

            }
        });

    }

    public String getAddress(Context context, double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);

            String add = obj.getAddressLine(0);

            return add;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }


    public void sendPost(String selectedLocation, String latitute, String longitute) {
        mAPIService.savePost(selectedLocation, latitute, longitute).enqueue(new Callback<ResponseClass>() {
            @Override
            public void onResponse(Call<ResponseClass> call, Response<ResponseClass> response) {

                if (response.isSuccessful()) {
                    String location = response.body().getSelectedLocation();
                    Log.i(TAG, "post submitted to API." + response.body().toString());
                    Toast.makeText(MapsActivity.this, "Location successfully updated to " + location, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseClass> call, Throwable t) {
                Log.e(TAG, "Unable to submit post to API.");
                Log.e(TAG, t.getMessage());
            }
        });
    }

}
