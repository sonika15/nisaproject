package com.project.test.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.project.test.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import androidx.core.app.NotificationCompat;

public class MyService extends Service {
    private Handler handlerUpdateLocation;

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service is Destroyed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (handlerUpdateLocation == null) {
            handlerUpdateLocation = new Handler();
            handlerUpdateLocation.post(runnableUpdateLocation);
        }

        return START_NOT_STICKY;
    }


    private Runnable runnableUpdateLocation = new Runnable() {

        @Override
        public void run() {
            Random r = new Random();
            int randomNumber = r.nextInt(10000);
            showNotification("Price Changed", "New Estimated Cost is " + String.valueOf(randomNumber));
            Log.wtf("price", "changed");
            handlerUpdateLocation.postDelayed(this, 60000); //show notification in every 1 minute
        }
    };

    private void showNotification(String task, String desc) {
        NotificationManager manager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "task_channel";
        String channelName = "task_name";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new
                    NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT);
            manager.createNotificationChannel(channel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                .setContentTitle(task)
                .setContentText(desc)
                .setSmallIcon(R.mipmap.ic_launcher);
        manager.notify(1, builder.build());
    }
}
