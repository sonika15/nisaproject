package com.project.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseClass {

    @SerializedName("selectedLocation")
    private String selectedLocation;


    @SerializedName("latitute")
    private String latitute;


    @SerializedName("longitutue")
    private String longitutue;



    public String getSelectedLocation() {
        return selectedLocation;
    }

    public void setSelectedLocation(String title) {
        this.selectedLocation = title;
    }

    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String body) {
        this.latitute = body;
    }

    public String getLongitutue() {
        return longitutue;
    }

    public void setLongitutue(String userId) {
        this.longitutue = userId;
    }


    @Override
    public String toString() {
        return "Post{" +
                "selectedLocation='" + selectedLocation + '\'' +
                ", latitute='" + latitute + '\'' +
                ", longitutue=" + longitutue +
                '}';
    }


}
