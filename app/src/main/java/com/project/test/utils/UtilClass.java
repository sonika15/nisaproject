package com.project.test.utils;

import com.project.test.NetworkCall;

public class UtilClass {

    public static final String BASE_URL = "https://reqres.in/api/";

    public static NetworkCall getAPIService() {

        return RetrofitClass.getClient(BASE_URL).create(NetworkCall.class);
    }
}
